﻿using System.Collections.Generic;

namespace EnjoyDigitalUniversity.Models
{
    public class CourseListingViewModel
    {
        public IEnumerable<Course> Type { get; set; }

        public IEnumerable<Course> Courses { get; set; }

        public string Department { get; set; }
    }    
}